import React from 'react'
import './DoneView.scss'

class DoneView extends React.Component {
  constructor (props) {
    super(props)

    this.onFacebookShareClick = this.onFacebookShareClick.bind(this)
  }

  onFacebookShareClick () {
    let { protocol, hostname, port } = window.location
    let url = `${protocol}//${hostname}${port !== 443 && port !== 80 ? port : ''}/issues/${this.props.params.issue}`

    window.FB.ui({
      method: 'share',
      href: url,
      quote: this.props.shareQuote
    }, (response) => {})
  }

  componentWillMount () {
    let issue = this.props.params.issue
    this.props.loadIssueBySlug(issue)
    this.props.setHeaderSubtext('Thanks! Now tell others to Tell Congress!')
  }

  render () {
    return (
      <div className={'done-view'}>
        <div className={'container'}>
          <h2>Share us with your friends</h2>
          <button onClick={this.onFacebookShareClick} className='fb-button btn btn-primary btn-lg'>
            <i className='fa fa-facebook' /><span>Share on Facebook</span>
          </button>
        </div>
      </div>
    )
  }
}

DoneView.propTypes = {
  loadIssueBySlug     : React.PropTypes.func.isRequired,
  params              : React.PropTypes.object.isRequired,
  shareQuote          : React.PropTypes.string,
  setHeaderSubtext    : React.PropTypes.func
}

export default DoneView
