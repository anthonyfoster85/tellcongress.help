import { injectReducer } from '../../store/reducers'

export default (store) => ({
  path : 'issues/:issue/done',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Done = require('./containers/DoneContainer').default
      const issueReducer = require('../Issues/modules/issues').issueReducer

      injectReducer(store, { key: 'issues', reducer: issueReducer })

      /*  Return getComponent   */
      cb(null, Done)

    /* Webpack named bundle   */
    }, 'done')
  }
})
