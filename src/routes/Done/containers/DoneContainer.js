import { connect } from 'react-redux'
import DoneView from '../components/DoneView'
import { loadIssueBySlug } from '../../Issues/modules/issues'
import { setHeaderSubtext } from '../../../store/layout'

const mapDispatchToProps = {
  loadIssueBySlug,
  setHeaderSubtext
}

const mapStateToProps = (state, ownProps) => {
  let issue = state.issues.collection[ownProps.params.issue]
  return {
    shareQuote: `Let's tell Congress ${issue ? issue.subtext : ''}`
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DoneView)
