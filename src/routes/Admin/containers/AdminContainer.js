import { connect } from 'react-redux'
import AdminView from '../components/AdminView'

const mapDispatchToProps = {}
const mapStateToProps = () => ({})

export default connect(mapStateToProps, mapDispatchToProps)(AdminView)
