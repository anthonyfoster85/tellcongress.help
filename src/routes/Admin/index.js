export default (store) => ({
  path : 'admin',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Admin = require('./containers/AdminContainer').default
      // const legislatorReducer = require('./modules/issues').legislatorReducer
      // const issueReducer = require('./modules/issues').issueReducer

      /*  Add the reducer to the store on key 'legislators'  */
      // injectReducer(store, { key: 'legislators', reducer: legislatorReducer })
      // injectReducer(store, { key: 'issues', reducer: issueReducer })
      /*  Return getComponent   */
      cb(null, Admin)

    /* Webpack named bundle   */
    }, 'admin')
  }
})
