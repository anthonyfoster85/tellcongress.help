import React from 'react'
import './AdminView.scss'

class AdminView extends React.Component {

  render () {
    return (
      <div className={'admin-view'}>
        <div className={'container'}>
          <h1>Admin Page</h1>
        </div>
      </div>
    )
  }
}

export default AdminView
