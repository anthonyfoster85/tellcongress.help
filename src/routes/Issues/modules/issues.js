import Promise from 'Promise'
import environment from 'environment'

// ------------------------------------
// Constants
// ------------------------------------
export const GET_LEGISLATORS_BY_ZIP_BEGIN = 'GET_LEGISLATORS_BY_ZIP_BEGIN'
export const GET_LEGISLATORS_BY_ZIP_SUCCESS = 'GET_LEGISLATORS_BY_ZIP_SUCCESS'
export const GET_LEGISLATORS_BY_ZIP_ERROR = 'GET_LEGISLATORS_BY_ZIP_ERROR'

export const GET_LEGISLATORS_BY_LOCATION_BEGIN = 'GET_LEGISLATORS_BY_LOCATION_BEGIN'
export const GET_LEGISLATORS_BY_LOCATION_SUCCESS = 'GET_LEGISLATORS_BY_LOCATION_SUCCESS'
export const GET_LEGISLATORS_BY_LOCATION_ERROR = 'GET_LEGISLATORS_BY_LOCATION_ERROR'

export const GET_ISSUE_BY_SLUG_BEGIN = 'GET_ISSUE_BY_SLUG_BEGIN'
export const GET_ISSUE_BY_SLUG_SUCCESS = 'GET_ISSUE_BY_SLUG_SUCCESS'
export const GET_ISSUE_BY_SLUG_ERROR = 'GET_ISSUE_BY_SLUG_ERROR'

export const GET_ISSUE_TEMPLATE_BY_SLUG_BEGIN = 'GET_ISSUE_TEMPLATE_BY_SLUG_BEGIN'
export const GET_ISSUE_TEMPLATE_BY_SLUG_SUCCESS = 'GET_ISSUE_TEMPLATE_BY_SLUG_SUCCESS'
export const GET_ISSUE_TEMPLATE_BY_SLUG_ERROR = 'GET_ISSUE_TEMPLATE_BY_SLUG_ERROR'

// ------------------------------------
// Actions
// ------------------------------------
export const loadLegislatorsForZipBegin = (zip) =>
  ({ type: GET_LEGISLATORS_BY_ZIP_BEGIN, payload: zip })
export const loadLegislatorsForZipSuccess = (legislators) =>
  ({ type: GET_LEGISLATORS_BY_ZIP_SUCCESS, payload: legislators })
export const loadLegislatorsForZipError = (zip) =>
  ({ type: GET_LEGISLATORS_BY_ZIP_ERROR, payload: zip })

export const loadLegislatorsForLocationBegin = (latitude, longitude) =>
  ({ type: GET_LEGISLATORS_BY_LOCATION_BEGIN, payload: { latitude, longitude } })
export const loadLegislatorsForLocationSuccess = (legislators) =>
  ({ type: GET_LEGISLATORS_BY_LOCATION_SUCCESS, payload: legislators })
export const loadLegislatorsForLocationError = (latitude, longitude) =>
  ({ type: GET_LEGISLATORS_BY_LOCATION_ERROR, payload: { latitude, longitude } })

export const loadIssueBySlugBegin = (slug) =>
  ({ type: GET_ISSUE_BY_SLUG_BEGIN, payload: slug })
export const loadIssueBySlugSuccess = (issue) =>
  ({ type: GET_ISSUE_BY_SLUG_SUCCESS, payload: issue })
export const loadIssueBySlugError = (slug) =>
  ({ type: GET_ISSUE_BY_SLUG_ERROR, payload: slug })

export const loadIssueTemplateBySlugBegin = (slug) =>
  ({ type: GET_ISSUE_TEMPLATE_BY_SLUG_BEGIN, payload: slug })
export const loadIssueTemplateBySlugSuccess = (issue) =>
  ({ type: GET_ISSUE_TEMPLATE_BY_SLUG_SUCCESS, payload: issue })
export const loadIssueTemplateBySlugError = (slug) =>
  ({ type: GET_ISSUE_TEMPLATE_BY_SLUG_ERROR, payload: slug })

const apiFetch = (path, options) => fetch(`${environment.api_url}${path}`, options)

export function loadLegislatorsForZip (zip) {
  return (dispatch) => {
    dispatch(loadLegislatorsForZipBegin(zip))
    return apiFetch(`/api/v1/congress/legislators/${zip}`)
    .then(checkStatus)
    .then((response) => response.json())
    .then((legislators) => {
      dispatch(loadLegislatorsForZipSuccess(legislators))
    }, () => {
      dispatch(loadLegislatorsForZipError(zip))
    })
  }
}

export function loadLegislatorsForLocation (latitude, longitude) {
  return (dispatch) => {
    dispatch(loadLegislatorsForLocationBegin(latitude, longitude))
    return apiFetch(`/api/v1/congress/legislators/${latitude}/${longitude}`)
    .then(checkStatus)
    .then((response) => response.json())
    .then((legislators) => {
      dispatch(loadLegislatorsForLocationSuccess(legislators))
    }, () => {
      dispatch(loadLegislatorsForLocationError(latitude, longitude))
    })
  }
}

export function loadIssueBySlug (slug) {
  return (dispatch) => {
    dispatch(loadIssueBySlugBegin(slug))
    return apiFetch(`/api/v1/issues/${slug}`)
    .then(checkStatus)
    .then((response) => response.json())
    .then((issue) => {
      dispatch(loadIssueBySlugSuccess(issue))
    }, () => {
      dispatch(loadIssueBySlugError(slug))
    })
  }
}

export function loadIssueTemplateBySlug (slug) {
  return (dispatch) => {
    dispatch(loadIssueTemplateBySlugBegin(slug))
    return apiFetch(`/api/v1/issues/${slug}/template`)
    .then(checkStatus)
    .then((response) => response.json())
    .then((templateJson) => {
      dispatch(loadIssueTemplateBySlugSuccess({ issueSlug: slug, template : templateJson.template }))
    }, () => {
      dispatch(loadIssueTemplateBySlugError(slug))
    })
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const LEGISLATOR_ACTION_HANDLERS = {
  [GET_LEGISLATORS_BY_ZIP_BEGIN]: function (state) {
    return Object.assign({}, state, { loading: true })
  },

  [GET_LEGISLATORS_BY_ZIP_SUCCESS]: function (state, action) {
    return Object.assign({}, state, { loading: false, list: action.payload })
  },

  [GET_LEGISLATORS_BY_ZIP_ERROR]: function (state) {
    return Object.assign({}, state, { loading: true })
  },

  [GET_LEGISLATORS_BY_LOCATION_BEGIN]: function (state) {
    return Object.assign({}, state, { loading: true })
  },

  [GET_LEGISLATORS_BY_LOCATION_SUCCESS]: function (state, action) {
    return Object.assign({}, state, { list: action.payload })
  },

  [GET_LEGISLATORS_BY_LOCATION_ERROR]: function (state) {
    return Object.assign({}, state, { loading: false })
  }
}

const ISSUES_ACTION_HANDLERS = {
  [GET_ISSUE_BY_SLUG_BEGIN]: function (state) {
    return Object.assign({}, state, { loading: true })
  },

  [GET_ISSUE_BY_SLUG_SUCCESS]: function (state, action) {
    let issue = action.payload
    return Object.assign({}, state, {
      loading: false,
      collection: {
        [issue.issueSlug]: issue
      }
    })
  },

  [GET_ISSUE_BY_SLUG_ERROR]: function (state) {
    return Object.assign({}, state, { loading: true })
  },

  [GET_ISSUE_TEMPLATE_BY_SLUG_BEGIN]: function (state) {
    return Object.assign({}, state, { loading: true })
  },

  [GET_ISSUE_TEMPLATE_BY_SLUG_SUCCESS]: function (state, action) {
    let { issueSlug, template } = action.payload
    let newIssue = Object.assign({}, state.collection[issueSlug], { template })
    return Object.assign({}, state, {
      loading: false,
      collection: {
        [issueSlug]: newIssue
      }
    })
  },

  [GET_ISSUE_TEMPLATE_BY_SLUG_ERROR]: function (state) {
    return Object.assign({}, state, { loading: true })
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
export function legislatorReducer (state = { loading: false, list: [] }, action) {
  const handler = LEGISLATOR_ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

export function issueReducer (state = { loading: false, collection: {} }, action) {
  const handler = ISSUES_ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

function checkStatus (response) {
  if (response.status >= 200 && response.status < 300) {
    return response
  } else {
    var error = new Error(response.statusText)
    error.response = response
    throw error
  }
}

export function getGeoLocation () {
  return new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition((position) => resolve({
      latitude: position.coords.latitude,
      longitude: position.coords.longitude
    }), () => reject())
  })
}

export function gmailRedirect (recaptcha, state) {
  let stateParam = state ? `?state=${encodeURIComponent(JSON.stringify(state))}` : ''

  let headers = {
    authorization: recaptcha
  }

  return apiFetch(`/api/v1/gmail/redirectUrl${stateParam}`, { headers })
    .then(checkStatus)
    .then((response) => response.json())
    .then((url) => { window.location = url })
}
