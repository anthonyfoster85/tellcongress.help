import React from 'react'
import './LegislatorList.scss'

export class LegislatorList extends React.Component {
  renderRows () {
    return this.props.legislators.map((l, index) => (
      <tr key={index}>
        <td>{`${l.first_name} ${l.last_name}`}</td>
        <td>{l.chamber[0].toUpperCase() + l.chamber.slice(1)}</td>
      </tr>
      )
    )
  }

  renderTable () {
    if (!this.props.legislators) {
      return
    }

    return (
      <table className={'table'}>
        <thead>
          <tr>
            <th>Name</th>
            <th>Chamber</th>
          </tr>
        </thead>
        <tbody>
          { this.renderRows() }
        </tbody>
      </table>
    )
  }

  render () {
    return (
      <div className={'legislator-list'}>
        { this.renderTable() }
      </div>
    )
  }
}

LegislatorList.propTypes = {
  legislators : React.PropTypes.array
}

export default LegislatorList
