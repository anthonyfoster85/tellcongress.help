import React from 'react'
import { gmailRedirect } from '../modules/issues'

export class GmailButton extends React.Component {
  constructor (props) {
    super(props)

    this.handleClick = this.handleClick.bind(this)
  }

  isDisabled () {
    return this.props.disabled || !this.props.recaptcha
  }

  handleClick () {
    if (!this.isDisabled()) {
      gmailRedirect(this.props.recaptcha, this.props.state)
    }
  }

  render () {
    let className = 'btn btn-primary btn-lg'
    if (this.isDisabled()) {
      className += ' disabled'
    }

    return (
      <button className={className} onClick={this.handleClick}>Email with GMail</button>
    )
  }
}

GmailButton.propTypes = {
  state     : React.PropTypes.object,
  disabled  : React.PropTypes.bool,
  recaptcha : React.PropTypes.string
}

export default GmailButton
