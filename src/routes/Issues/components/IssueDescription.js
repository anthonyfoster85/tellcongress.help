import React from 'react'
import './IssueDescription.scss'

export class IssueDescription extends React.Component {
  render () {
    let html = { __html: this.props.description }
    return (
      <div className={'issue-description'} dangerouslySetInnerHTML={html} />
    )
  }
}

IssueDescription.propTypes = {
  description     : React.PropTypes.string
}

export default IssueDescription
