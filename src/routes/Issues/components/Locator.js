import React from 'react'
import { getGeoLocation } from '../modules/issues'
import './Locator.scss'

function isValid (zip) {
  return zip && zip.length === 5 && zip.match(/^\d+$/)
}

export class Locator extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      zip: ''
    }

    this.handleLocationClick = this.handleLocationClick.bind(this)
    this.handleZipChange = this.handleZipChange.bind(this)
  }

  handleLocationClick () {
    getGeoLocation().then((position) => {
      if (this.props.onLocationChange) {
        this.props.onLocationChange(position)
      }
    })
  }

  handleZipChange (event) {
    if (event.target.value.length <= 5) {
      let zip = event.target.value
      this.setState({ zip })
      if (this.props.onZipChange && isValid(zip)) {
        this.props.onZipChange(zip)
      }
    }
  }

  render () {
    return (
      <div className={'locator form-inline form-group'}>
        <button className={'btn btn-primary btn-lg'} onClick={this.handleLocationClick}>From Your Location</button>
        <h3 className={'or'}>Or</h3>
        <input className={'form-control input-lg'} type='text' value={this.state.zip}
          placeholder='5 digit zip' onChange={this.handleZipChange} onKeyPress={this.handleZipKeyPress} />
      </div>
    )
  }
}

Locator.propTypes = {
  onLocationChange  : React.PropTypes.func,
  onZipChange       : React.PropTypes.func
}

export default Locator
