import React from 'react'
import './IssueTemplate.scss'

function escapeHtml (html) {
  let text = document.createTextNode(html)
  var div = document.createElement('div')
  div.appendChild(text)
  return div.innerHTML
}

export class IssueTemplate extends React.Component {
  render () {
    let template = this.props.template
        .replace('{{CongressPerson}}', 'Mr. or Ms. Congress Person')
        .replace('{{Signature}}', this.props.name ? escapeHtml(this.props.name) : '')
    let html = { __html: template }
    return (
      <div className={'issue-template well'} dangerouslySetInnerHTML={html} />
    )
  }
}

IssueTemplate.propTypes = {
  template : React.PropTypes.string.isRequired,
  name : React.PropTypes.string
}

export default IssueTemplate
