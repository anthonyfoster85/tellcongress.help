import React from 'react'
import ReactDOM from 'react-dom'
import Recaptcha from 'react-recaptcha'
import IssueDescription from './IssueDescription'
import IssueTemplate from './IssueTemplate'
import Locator from './Locator'
import LegislatorList from './LegislatorList'
import GmailButton from './GmailButton'
import './IssuesView.scss'

function scrollToComponent (ref) {
  if (ref) {
    let node = ReactDOM.findDOMNode(ref)
    node.scrollIntoView()
  }
}

class IssuesView extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      name: ''
    }

    this.onRecaptchaVerify = this.onRecaptchaVerify.bind(this)
    this.onZipChange = this.onZipChange.bind(this)
    this.onLocationChange = this.onLocationChange.bind(this)
    this.onTellCongressClick = this.onTellCongressClick.bind(this)
    this.onNameChange = this.onNameChange.bind(this)
  }

  componentWillMount () {
    let issue = this.props.params.issue
    this.props.loadIssueBySlug(issue)
  }

  componentWillReceiveProps (nextProps) {
    if (this.props.subtext !== nextProps.subtext) {
      this.props.setHeaderSubtext(nextProps.subtext)
    }
  }

  onRecaptchaVerify (response) {
    this.setState({ recaptcha: response })
  }

  onZipChange (zip) {
    let value = this.props.loadLegislatorsForZip(zip)
    if (value instanceof Promise) {
      value.then(() => {
        scrollToComponent(this._legislatorList)
      })
    }
  }

  onNameChange (event) {
    this.setState({ name: event.target.value })
  }

  onLocationChange (position) {
    let value = this.props.loadLegislatorsForLocation(position.latitude, position.longitude)
    if (value instanceof Promise) {
      value.then(() => {
        scrollToComponent(this._legislatorList)
      })
    }
  }

  onTellCongressClick () {
    let issue = this.props.params.issue
    let value = this.props.loadIssueTemplateBySlug(issue)
    if (value instanceof Promise) {
      value.then(() => {
        scrollToComponent(this._templateContainer)
      })
    }
  }

  renderLegislatorList () {
    let legislatorList = this.props.legislators && this.props.legislators.length > 0
      ? <LegislatorList ref={c => { this._legislatorList = c }} legislators={this.props.legislators} /> : null

    if (!legislatorList) {
      return
    }

    return (
      <div className={'legislator-container'}>
        <h2>Your legislators are:</h2>
        {legislatorList}
        <button className={'btn btn-primary btn-lg'} onClick={this.onTellCongressClick}>
          Email 'em!
        </button>
      </div>
    )
  }

  renderTemplate () {
    let template = this.props.template
    if (!template) {
      return
    }

    let doneUrl = `${window.location.origin}/issues/health/done`
    let issueId = this.props.issueId
    let to = this.props.legislators.map((c) => ({
      name: `${c.first_name} ${c.last_name}`,
      email: c.email,
      gender: c.gender
    }))

    return (
      <div ref={c => { this._templateContainer = c }} className={'template-container'}>
        <IssueTemplate template={this.props.template} name={this.state.name} />
        <input className={'form-control input-lg'} type='text' value={this.state.name}
          placeholder='Your signature (Required)' onChange={this.onNameChange} />
        <Recaptcha
          sitekey='6Ld9BhEUAAAAAD7z2nY_GWBEZvR5AKYPN2wXqXOA'
          render='explicit'
          verifyCallback={this.onRecaptchaVerify}
          onloadCallback={() => {}}
        />
        <GmailButton
          recaptcha={this.state.recaptcha}
          disabled={!this.state.name}
          state={{
            from: this.state.name ? this.state.name : '',
            issue: this.props.params.issue,
            issueId,
            doneUrl,
            to
          }} />
      </div>
    )
  }

  render () {
    return (
      <div className={'issues-view'}>
        <div className={'container'}>
          <IssueDescription description={this.props.description} />
          <Locator onZipChange={this.onZipChange} onLocationChange={this.onLocationChange} />
          { this.renderLegislatorList() }
          { this.renderTemplate() }
        </div>
      </div>
    )
  }
}

IssuesView.propTypes = {
  params                     : React.PropTypes.object.isRequired,
  description                : React.PropTypes.string,
  issueId                    : React.PropTypes.number,
  legislators                : React.PropTypes.array.isRequired,
  loadLegislatorsForZip      : React.PropTypes.func.isRequired,
  loadLegislatorsForLocation : React.PropTypes.func.isRequired,
  loadIssueBySlug            : React.PropTypes.func.isRequired,
  loadIssueTemplateBySlug    : React.PropTypes.func.isRequired,
  setHeaderSubtext           : React.PropTypes.func,
  subtext                    : React.PropTypes.string,
  template                   : React.PropTypes.string
}

export default IssuesView
