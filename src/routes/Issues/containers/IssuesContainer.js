import { connect } from 'react-redux'
import {
  loadLegislatorsForLocation,
  loadLegislatorsForZip,
  loadIssueBySlug,
  loadIssueTemplateBySlug } from '../modules/issues'
import { setHeaderSubtext } from '../../../store/layout'
import IssuesView from '../components/IssuesView'

const mapDispatchToProps = {
  loadLegislatorsForLocation,
  loadLegislatorsForZip,
  loadIssueBySlug,
  loadIssueTemplateBySlug,
  setHeaderSubtext
}

const mapStateToProps = (state, ownProps) => {
  let issue = state.issues.collection[ownProps.params.issue]
  let issueId, description, subtext, template
  if (issue) {
    issueId = issue.issueId
    description = issue.description
    subtext = issue.subtext
    template = issue.template
  }

  return {
    issueId,
    description,
    subtext,
    template,
    legislators: state.legislators.list
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(IssuesView)
