// import Promise from 'Promise'
// import environment from 'environment'
import { setHeaderSubtext } from '../../../store/layout'

// ------------------------------------
// Constants
// ------------------------------------
export const HOMEVIEW_WILL_MOUNT = 'HOMEPAGE_WILL_MOUNT'

// const apiFetch = (path) => fetch(`${environment.api_url}${path}`)

export function homeWillMount () {
  return (dispatch) => {
    dispatch(setHeaderSubtext('to make common sense decisions'))
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const HOME_ACTION_HANDLERS = {
  [HOMEVIEW_WILL_MOUNT]: function (state) {
    return Object.assign({}, state, { loading: true })
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
export function homeReducer (state = { }, action) {
  const handler = HOME_ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}

/* function checkStatus (response) {
  if (response.status >= 200 && response.status < 300) {
    return response
  } else {
    var error = new Error(response.statusText)
    error.response = response
    throw error
  }
} */
