import React from 'react'
import { Link } from 'react-router'
import './HomeView.scss'

export class HomeView extends React.Component {
  // constructor (props) {
    // super(props)
  // }

  componentWillMount () {
    this.props.homeWillMount()
  }

  render () {
    return (
      <div className={'container home'}>
        <section className={'issues'}>
          <h1>Issues</h1>
          <ul>
            <li><Link to={'/issues/health'}>Healthcare</Link></li>
          </ul>
        </section>
        <section className={'about'}>
          <h1>It's our duty</h1>
          <p>
            Congress is elected by us to work for us. What does this mean? It means it's our
            responsibility to make sure they know where we stand on the issues.
          </p>
          <h1>What can we do?</h1>
          <p>
            The goal of TellCongress.Help is to empower you, the citizen, to keep your congress people
            accountable! We target tough issues, and give you the ability to quickly and easily reach out to your
            members of congress.  You can then share the page, and share the duty with others.
          </p>
        </section>
      </div>
    )
  }
}

HomeView.propTypes = {
  homeWillMount : React.PropTypes.func
}

export default HomeView
