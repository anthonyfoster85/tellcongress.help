import { connect } from 'react-redux'
import { homeWillMount } from '../modules/home'
import HomeView from '../components/HomeView'

const mapDispatchToProps = {
  homeWillMount
}

const mapStateToProps = (state, ownProps) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeView)
