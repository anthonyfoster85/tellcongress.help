import React from 'react'
import Header from '../../components/Header'
import './CoreLayout.scss'
import '../../styles/core.scss'

export const CoreLayout = ({ children, subtext }) => (
  <div>
    <div className='core-layout__viewport'>
      <Header subText={subtext} />
      {children}
    </div>
  </div>
)

CoreLayout.propTypes = {
  children : React.PropTypes.element.isRequired,
  subtext : React.PropTypes.string
}

export default CoreLayout
