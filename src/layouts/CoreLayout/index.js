import { connect } from 'react-redux'
import CoreLayout from './CoreLayout'

const mapDispatchToProps = {
}

const mapStateToProps = (state) => {
  return {
    subtext: state.layout.subtext
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CoreLayout)
