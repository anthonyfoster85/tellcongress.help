import React from 'react'
import './Header.scss'

class Header extends React.Component {
  render () {
    let style = {
    }

    return (
      <header style={style}>
        <div className={'content'} >
          <div className={'text-container'}>
            <h1>Let's Tell Congress</h1>
            <h2>{this.props.subText}</h2>
          </div>
        </div>
      </header>
    )
  }
}

Header.propTypes = {
  subText     : React.PropTypes.string
}

export default Header
