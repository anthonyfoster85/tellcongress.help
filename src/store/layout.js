export const SET_HEADER_SUBTEXT = 'SET_HEADER_SUBTEXT'

export function changeSubtext (subtext = '') {
  return {
    type    : SET_HEADER_SUBTEXT,
    payload : subtext
  }
}

export const setHeaderSubtext = (subtext) => {
  return (dispatch) => dispatch(changeSubtext(subtext))
}

const CORE_ACTION_HANDLERS = {
  [SET_HEADER_SUBTEXT]: (state, action) => Object.assign({}, state, { subtext: action.payload })
}

export function layoutReducer (state = { subtext: '' }, action) {
  const handler = CORE_ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
